<?php
include "action/koneksi.php";
include_once "action/fungsi.php";
if(isset($_GET['provinsiId'])) {
    $provinsiId = (int)$_GET['provinsiId'];
    $kabupaten = kabupaten("SELECT * FROM kabupaten WHERE provinsiId=".$provinsiId);        
?>
    <select name="kabupaten" id="kabupaten">
        <?php
        foreach ($kabupaten as $key) :
        ?>
        <option value="<?= $key['kabupatenId'] ?>"><?= $key['kabupatenNama']; ?></option>
        <?php endforeach; ?>
    </select>
<?php  }elseif(isset($_GET['kabupatenId'])) { 
    $kabupatenId = (int)$_GET['kabupatenId'];
    $kecamatan = kecamatan("SELECT * FROM kecamatan WHERE kabupatenId=".$kabupatenId);
?>
    <select name="kecamatan" id="kecamatan">
        <?php
        foreach ($kecamatan as $key) :
        ?>
        <option value="<?= $key['kecamatanId'] ?>"><?= $key['kecamatanNama']; ?></option>
        <?php endforeach; ?>
    </select>

<?php } ?>